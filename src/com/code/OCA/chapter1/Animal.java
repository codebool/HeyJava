package com.code.OCA.chapter1;

import java.util.Random;

public class Animal<main> {
    String name;

    public String getName() {
        return this.name;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public static void main(String[] args) {
        System.out.println("Hello Animals.");
        Animal dog = new Animal();
        dog.setName("Mira");
        System.out.println(dog.name);

        System.out.println(args[0]);
        System.out.println(args[1]);

        Random r = new Random();
        System.out.println(r.nextInt(2));
    }
}
