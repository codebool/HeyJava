package com.code.OCA.chapter1;

public class Chicken {
    // Redundant
    private String name = "Ben";
    {
        System.out.println(name);
    }

    public Chicken() {
        this.name = "Bool";
        {
            System.out.println("Constructor: " + name);
        }
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public static void main(String[] args) {
        Chicken c = new Chicken();
        System.out.println(c.name);
        c.setName("123");
        System.out.println(c.name);
    }

    {
        System.out.println("Out of Main Method");
    }
}
