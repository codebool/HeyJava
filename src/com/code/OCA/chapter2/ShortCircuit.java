package com.code.OCA.chapter2;

public class ShortCircuit {
    public static void main(String[] args) {
        int a = 4;
        int b = 7;

        int c = (a>=4) ? 11 : ++b;
        System.out.println("c: " + c);
        System.out.println("b: " + b);

        int d = (a<4) ? 0 : b++;
        System.out.println("d: " + d);
        System.out.println("b: " + b);

        int x = -1;
        int y = 7;
        int z = x=true? 1 : 2;
//        System.out.println(x=true);
        System.out.println("z: " + z);
    }
}
