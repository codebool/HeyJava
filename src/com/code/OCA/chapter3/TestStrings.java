package com.code.OCA.chapter3;

import java.util.ArrayList;
import java.util.Arrays;

public class TestStrings {
    public static void main(String[] args) {
        String name1 = "Java";
        String name2 = new String("Java");
        String name3 = "Java";

        boolean x = (name3 == name1);

        System.out.println(x);
        System.out.println(name1);
        System.out.println(name2);

        int[] numbersOne = {2, 4, 6, 8};
        System.out.println(Arrays.binarySearch(numbersOne, 9));

        int[] numbersTwo = {3, 2, 1};
        System.out.println(Arrays.binarySearch(numbersTwo, 2));
        System.out.println(Arrays.binarySearch(numbersTwo, 3));

        ArrayList list = new ArrayList();
        System.out.println(list.add('x'));
        System.out.println(list);

        int primitive = Integer.parseInt("123");
        Integer wrapper = Integer.valueOf("123");
    }
}
