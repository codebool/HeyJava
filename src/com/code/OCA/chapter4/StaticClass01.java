package com.code.OCA.chapter4;

public class StaticClass01 {
    public static int counter = 12;
    private static String name = "Hello Kitty";
    public static void reader() {
        System.out.println(name);
    }

    public static void main (String[] args) {
        System.out.println(counter);
        reader();
    }
}
