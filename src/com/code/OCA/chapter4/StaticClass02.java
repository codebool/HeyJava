package com.code.OCA.chapter4;

public class StaticClass02 {
    private static final int NUM_SECONDS;
    static {
        int HOURS = 60;
        int MINPERHOUR = 60;
        NUM_SECONDS = HOURS * MINPERHOUR;
    }
}
