package com.code.OCA.chapter5;

public class ClassDesigh01 extends Animal {
    private void roar() {
        System.out.println("The age is: " + getAge());
    }

//    public ClassDesigh01() {
//        super(10);
//    }

    public ClassDesigh01() {
        super(22);
    }

    public static void main(String args[]) {
        ClassDesigh01 c = new ClassDesigh01();
        c.setAge(22);
        c.roar();
    }
}

class Animal {
    private int age;
    public Animal(int age) {
        super();
        this.age = age;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}
