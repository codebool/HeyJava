package com.code.OCA.chapter5;

public class HiddenStaticMethods extends Bear {
    public static void eat() {
        System.out.println("Child class");
    }

    public static void main(String[] args) {
        Bear.eat();
        HiddenStaticMethods.eat();
    }
}

class Bear {
    public static void eat() {
        System.out.println("Bear class");
    }
}