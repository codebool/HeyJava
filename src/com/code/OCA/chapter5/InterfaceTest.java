package com.code.OCA.chapter5;

public interface InterfaceTest {
    void fly(int speed);
    abstract void takeoff();
    public abstract double dive();
}

//==>
//public abstract interface InterfaceTest {
//    public abstract void fly(int speed);
//    public abstract void takeoff();
//    public abstract double dive();
//}

// Virtual Methods: is a method in which the specific implementation is not determined until runtime.