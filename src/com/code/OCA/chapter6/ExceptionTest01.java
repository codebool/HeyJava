package com.code.OCA.chapter6;

public class ExceptionTest01 {
    public static void main(String[] args) {
        try {
            hop();
        } catch(Exception e) {
            System.out.println("1: " + e);
            System.out.println("2: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static void hop() {
        throw new RuntimeException("RuntimeException from hop()");
    }
}

//runtime exception: (unchecked exception)
//    ArithmeticException
//    ArrayIndexOutOfBoundsException
//    ClassCastException
//    IllegalArgumentException
//    NullPointerException
//    NumberFormatException
//
//    Checked Exception:
//    FileNotFoundException
//    IOException
