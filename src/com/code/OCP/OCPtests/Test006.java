package com.code.OCP.OCPtests;

import java.sql.*;

public class Test006 {
    public final static String dbURL = "";
    public final static String username = "";
    public final static String password = "";

    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection(dbURL, username, password);
            String query = "Select * FROM Item WHERE ID = 110";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                System.out.println("ID:" + rs.getInt("Id"));
                System.out.println("Description:" + rs.getString("Descrip"));
                System.out.println("Price:" + rs.getDouble("Price"));
                System.out.println("Quantity:" + rs.getInt("Quantity"));
            }
        } catch (SQLException se) {
            System.out.println("Error");
        }
    }
}
