package com.code.OCP.OCPtests;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Test007 {
    public enum Continent {
        ASIA, EUROPE
    }
    String name;
    Continent region;

    public Test007(String na, Continent reg) {
        name = na;
        region = reg;
    }

    public String getName() {
        return name;
    }

    public Continent getRegion() {
        return region;
    }

    public static void main(String[] args) {

        List<Test007> couList = Arrays.asList(
                new Test007("Japan", Test007.Continent.ASIA),
                new Test007("Italy", Test007.Continent.EUROPE), new Test007("Germany", Test007.Continent.EUROPE));
        Map<Continent, List<String>> regionNames = couList.stream().
                collect(Collectors.groupingBy(Test007::getRegion,
                        Collectors.mapping(Test007::getName, Collectors.toList())));
        System.out.println(regionNames);
    }
}
//元素在Map物件中的順序，愈先建立群組的項目會排在愈後面，所以「ASIA」會排在「EUROPE」之後。元素在List物件中的順序就是原先集合物件的走訪順序，所以「Italy」在「Germany」之前。