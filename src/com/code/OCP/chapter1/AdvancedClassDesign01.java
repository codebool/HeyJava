package com.code.OCP.chapter1;

public class AdvancedClassDesign01 {
    public static void main(String[] args) {
        System.out.println("OCP: Advanced Class Design");
    }
}


//Java access modifier and optional modifier order ===>
//[ public | protected | private ]
//static
//abstract
//synchronized
//[ transient | volatile ]
//final
//native
//strictfp
//[ int | long | String | class | enum | interface etc. ]

// http://checkstyle.sourceforge.net/config_modifier.html
//public
//protected
//private
//abstract
//default
//static
//final
//transient
//volatile
//synchronized
//native
//strictfp