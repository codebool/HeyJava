package com.code.OCP.chapter1;

public class AnonymousInnerClassTest {
    abstract class SaleTodayOnly {
        abstract int dollarsOff();
    }
    public int admission(int basePrice) {
        SaleTodayOnly sale = new SaleTodayOnly() {
            @Override
            int dollarsOff() {
                return 3;
            }
        };
        return basePrice - sale.dollarsOff();
    }
}


// Note:
// Anonymous inner classes are required to extend an existing class or implement an existing interface

//public class AnonInner {
//    interface SaleTodayOnly {
//    int dollarsOff();
//    }
//    public int admission(int basePrice) {
//        SaleTodayOnly sale = new SaleTodayOnly() {
//            public int dollarsOff() { return 3; }
//        };
//        return basePrice - sale.dollarsOff();
//    }
//}
