package com.code.OCP.chapter1;

enum Season {
    SPRING{public String toString() {
        return "This is spring.";
    }}, SUMMER, FALL, WINTER
}

public class EnumTest01 {
    public static void main(String[] args) {
        for (Season s : Season.values()) {
            System.out.println(s.name() + " " + s.ordinal());
        }

        Season s1 = Season.valueOf("SPRING");
        System.out.println(s1);
    }
}
