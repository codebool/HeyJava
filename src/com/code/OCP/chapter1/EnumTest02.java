package com.code.OCP.chapter1;

enum WeekDay {

    //最好全大写，因为枚举类型的值是public static final的
    MON("Monday"), TUE("Tuesday"), WED("Wednesday"), THU("Thursday"),
    FRI("Friday"), SAT("Saturday"), SUN("Sunday");

    //定义枚举类型的属性
    private final String day;

    /*
     * 构造器只能是private,不允许有public构造器,这样可以保证外部代码无法新 构造枚举类的实例,
     * 因为我们知道枚举值是public static final的常量
     */
    private WeekDay(String day) {
        this.day = day;
        //验证获取枚举常量时会被调用
        System.out.println("The constructor is invoked ！");
    }

    //普通方法
    public String getDay() {
        return day;
    }
}

public class EnumTest02 {
    public static void main(String[] args) {

        //通过WeekDay.枚举常量 获取枚举常量，此时会初始化所有枚举常量，会调用构造器
        WeekDay weekDay = WeekDay.WED;//调用构造方法，输出结果：七句 The constructor is invoked ！

        //获取枚举属性，通过普通方法getDay()
        System.out.println(weekDay.getDay());//输出结果：Wednesday

        //enum常用方法

        //1.toString()方法，返回枚举常量的名称
        System.out.println(weekDay.toString());//输出结果：WED
        System.out.println(weekDay);//会调用toString()方法,输出结果：WED

        //2.name()方法,返回枚举常量的名称
        System.out.println(weekDay.name());//输出结果：WED

        //上面两种方法都是返回枚举常量的名称，但是API推荐使用toString()方法

        //3.values()静态方法，返回全部枚举常量的数组
        for (WeekDay wd : WeekDay.values()) {
            System.out.print(wd + " ");//输出结果：MON TUE WED THU FRI SAT SUN
        }

        //4.ordinal()方法返回枚举常量的序数（它在枚举声明中的位置，其中初始常量序数为零）
        System.out.println(weekDay.ordinal());//输出结果：2

        //5.1 valueOf(String name)静态方法，返回带指定名称的指定枚举类型的枚举常量(完全匹配)
        System.out.println(WeekDay.valueOf("SUN"));//输出结果：SUN

        //5.2 valueOf((Class<T> enumType,String name)静态方法，返回带指定名称的指定枚举类型的枚举常量(完全匹配)

        //使用方法1（类.class方法）
        System.out.println(WeekDay.valueOf(WeekDay.class, "MON"));

        //使用方法2（枚举常量.getgetDeclaringClass()方法返回与此枚举常量的枚举类型相对应的 Class 对象）
        //不能使用枚举常量.getClass()方法 注：java官方api有说明
        System.out.println(WeekDay.valueOf(weekDay.getDeclaringClass(), "SUN"));//输出结果：SUN

        //6.compareTo(E o) 比较此枚举与指定对象的顺序
        System.out.println(WeekDay.MON.compareTo(weekDay));//输出结果：-2

        //枚举类型可以在switch语句中使用 下面的输出结果：WED
        switch (weekDay) {
            case MON:
                System.out.println("MON");
                break;
            case TUE:
                System.out.println("TUE");
                break;
            case WED:
                System.out.println("WED");
                break;
            default:
                System.out.println("default");
        }
    }
}
