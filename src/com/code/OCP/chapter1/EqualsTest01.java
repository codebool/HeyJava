package com.code.OCP.chapter1;

public class EqualsTest01 {
    public static void main(String[] args) {
        String s1 = new String("lion");
        String s2 = new String("lion");
        // String does have an equals() method. It checks that the values are the same.
        System.out.println(s1.equals(s2));
        // Check if two variables refer to the same object
        System.out.println(s1 == s2);

        StringBuilder sb1 = new StringBuilder("lion");
        StringBuilder sb2 = new StringBuilder("lion");
        System.out.println(sb1.equals(sb2));

        StringBuilder sb3 = new StringBuilder("lion");
        StringBuilder sb4 = sb3;
        System.out.println(sb3 == sb4);
    }
}
