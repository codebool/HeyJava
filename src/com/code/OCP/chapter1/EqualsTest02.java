package com.code.OCP.chapter1;

public class EqualsTest02 {
    private int idNumber;
    private int age;
    private String name;

    public EqualsTest02(int idNumber, int age, String name) {
        this.idNumber = idNumber;
        this.age = age;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof EqualsTest02) || obj == null) {
            return false;
        }
        EqualsTest02 other = (EqualsTest02) obj;
        return this.idNumber == other.idNumber;
    }
}
