package com.code.OCP.chapter1;

public class HashCodeTest {
    private String rank;
    private String suit;

    public HashCodeTest(String r, String s) {
        if (r==null || s == null) {
            throw new IllegalArgumentException();
        }
        rank = r;
        suit = s;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof HashCodeTest) || obj == null) {
            return false;
        }
        HashCodeTest h = (HashCodeTest) obj;
        return rank.equals(h.rank) && suit.equals(h.suit);
    }

    public int hashCode() {
        return rank.hashCode();
    }

    public static void main(String[] args) {
        HashCodeTest h = new HashCodeTest("aaa", "bbb");
        System.out.println(h.hashCode());
        System.out.println(h.rank.hashCode());
    }
}
