package com.code.OCP.chapter1;

public class MemberInnerClassTest01 {
    private String greeting = "Hi";

    protected class Inner {
        public int repeat = 3;
        public void go() {
            for (int i=0; i<repeat; i++) {
                System.out.println(greeting);
            }
        }
    }

    public void callInner() {
        Inner inner = new Inner();
        inner.go();
    }

    // Option One
//    public static void main(String[] args) {
//        MemberInnerClassTest01 m = new MemberInnerClassTest01();
//        m.callInner();
//    }

    // Option Two
    public static void main(String[] args) {
        MemberInnerClassTest01 m = new MemberInnerClassTest01();
        Inner inner = m.new Inner();
        inner.go();
    }
}
