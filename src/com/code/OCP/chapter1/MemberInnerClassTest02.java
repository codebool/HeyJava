package com.code.OCP.chapter1;

public class MemberInnerClassTest02 {
    private int x = 10;
    class B {
        private int x = 20;
        class C {
            private int x = 30;
            public void allTheX() {
                System.out.println(x);
                System.out.println(this.x);
                System.out.println(B.this.x);
                System.out.println(MemberInnerClassTest02.this.x);
            }
        }
    }

    public static void main(String[] args) {
        MemberInnerClassTest02 a = new MemberInnerClassTest02();
        MemberInnerClassTest02.B b = a.new B();
        MemberInnerClassTest02.B.C c = b.new C();
        c.allTheX();
    }
}
