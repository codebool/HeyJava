package com.code.OCP.chapter1;

public class ToStringTest02 {
    private String name;
    private double weight;

    public ToStringTest02(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

//    @Override
//    public String toString() {
//        return name;
//    }

//    @Override
//    public java.lang.String toString() {
//        return super.toString();
//    }

//    @Override
//    public String toString() {
//        return ToStringBuilder.reflectionToString(this);
//    }

    public static void main(String[] args) {
        ToStringTest02 t = new ToStringTest02("Benjamin", 3000);
        System.out.println(t);
    }
}
