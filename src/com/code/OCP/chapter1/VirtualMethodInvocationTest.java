package com.code.OCP.chapter1;

public class VirtualMethodInvocationTest extends Animal {
    public static void main(String[] args) {
        Animal animal = new Lion();
        animal.printName();

        Lion lion = new Lion();
        lion.printName();
    }
}

abstract class Animal {
    String name = "???";
    public void printName() {
        System.out.println(name);
    }
}

class Lion extends Animal {
    String name = "Lion";

    @Override
    public void printName() {
        System.out.println(name);
    }
}