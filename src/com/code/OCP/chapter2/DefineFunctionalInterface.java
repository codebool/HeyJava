package com.code.OCP.chapter2;

// Java defines a functional interface as an interface that contains a single abstract method

@FunctionalInterface
interface Sprint {
    public void sprint(); // There is only declaration, so no curly brackets {}
}

public class DefineFunctionalInterface implements Sprint {
    public void sprint() {
        System.out.println("Animal is sprinting fast!");
    }
}
