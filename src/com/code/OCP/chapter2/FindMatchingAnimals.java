package com.code.OCP.chapter2;

class Animal {
    private String species;
    private boolean canHop;
    private boolean canSwim;
    public Animal(String speciesName, boolean hopper, boolean swimmer) {
        species = speciesName;
        canHop = hopper;
        canSwim = swimmer;
    }

    public boolean canHop() {
        return canHop;
    }

    public boolean isCanSwim() {
        return canSwim;
    }

    public String toString() {
        return species;
    }
}

interface CheckTrait {
    public boolean test(Animal a);
}

public class FindMatchingAnimals {
    private static void print(Animal animal, CheckTrait trait) { // Predicate<Animal> trait
        if(trait.test(animal)) {
            System.out.println(animal);
        }
    }

    public static void main(String[] args) {
        print(new Animal("fish", true, true), a -> a.canHop() );
        print(new Animal("kangaroo", true, true), a -> a.isCanSwim());
    }
}

// Data types for the input parameters of a lambda expression are optional.
// When one parameter has a data type listed, though, all parameters must provide a data type.
// And re-declare is not allowed
