package com.code.OCP.chapter3;

import java.util.ArrayList;
import java.util.List;

public class ArrayAndList001 {
    public static void main(String[] args) {
        List<String> l = new ArrayList<>();
        l.add("Apple");
        l.add("Beer");
        System.out.println("list: " + l);
        int ll = l.size();
        System.out.println("list size: " + ll);

        String[] a = new String[ll];
        System.out.println("array length: " + a.length);
        a[0] = l.get(0);
        a[1] = l.get(1);
        for (String s : a) {
            System.out.println(s);
        }
    }
}
