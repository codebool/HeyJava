package com.code.OCP.chapter3;

import java.util.Arrays;
import java.util.List;

public class ArrayAndList002 {
    public static void main(String[] args) {
        String[] array = {"Apple", "Beer", "Carrot"};
        for (String s : array) {
            System.out.println("array: " + s);
        }

        List<String> list = Arrays.asList(array);
        list.set(0, "Alex");
        list.set(1, "Benjamin");
        System.out.println("list: " + list);

        String[] array2 = (String[])list.toArray();
        for (String s : array2) {
            System.out.println(s);
        }
    }
}
