package com.code.OCP.chapter3;

import java.util.ArrayDeque;
import java.util.Queue;

public class ArrayDequeTest {
    public static void main(String[] args) {
        Queue<Integer> qeque = new ArrayDeque<>();
        System.out.println(qeque.offer(10));
        System.out.println(qeque.offer(4));
        System.out.println(qeque.peek());
        System.out.println(qeque.poll());
        System.out.println(qeque.poll());
        System.out.println(qeque.poll());

        System.out.println(qeque);
    }
}
