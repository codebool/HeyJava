package com.code.OCP.chapter3;

import java.util.ArrayList;
import java.util.List;

public class AutoboxTest {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(new Integer(3));
        System.out.println(numbers);

        Integer num1 = numbers.get(0);
        int num2 = numbers.get(1);
        System.out.println(num1 + "---" + num2);
    }
}
