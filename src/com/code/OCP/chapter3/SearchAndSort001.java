package com.code.OCP.chapter3;

import java.util.Arrays;

public class SearchAndSort001 {
    public static void main(String[] args) {
        int[] number = {3, 1, 7, 9, 5, 6};
        for (int i : number) {
            System.out.println("old: " + i);
        }
        System.out.println(Arrays.binarySearch(number, 1));

        // the number array has been updated
        Arrays.sort(number);
        for (int i : number) {
            System.out.println("new: " + i);
        }
        System.out.println(Arrays.binarySearch(number, 1));
    }
}
