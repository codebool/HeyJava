package com.code.OCP.chapter3;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SearchAndSort002 {
    public static void main(String[] args) {
        Integer[] array = {3, 4, 1, 2, 9, 7};
        List<Integer> list = Arrays.asList(array);
        System.out.println("old: " + list);
        Collections.sort(list);
        System.out.println("new: " + list);

        System.out.println(Collections.binarySearch(list, 1));
        System.out.println(Collections.binarySearch(list, 100));
    }
}