package com.code.OCP.chapter4;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class BiConsumerTest {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        BiConsumer<String, Integer> b1 = map::put;
        b1.accept("Benjamin", 666);

        BiConsumer<String, Integer> b2 = (k, v) -> map.put(k, v);
        b2.accept("Anna", 888);

        System.out.println(map);
    }
}
