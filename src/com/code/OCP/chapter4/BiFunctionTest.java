package com.code.OCP.chapter4;

import java.util.function.BiFunction;

public class BiFunctionTest {
    public static void main(String[] args) {
        BiFunction<String, String, String> b1 = String::concat;
        String s1 = b1.apply("Ben", "Cool");
        System.out.println(s1);

        BiFunction<String, String, String> b2 = (string, toAdd) -> string.concat(toAdd);
        String s2 = b2.apply("Apple", " Juice");
        System.out.println(s2);
    }
}
