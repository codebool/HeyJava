package com.code.OCP.chapter4;

import java.util.function.BiPredicate;

public class BiPredicateTest {
    public static void main(String[] args) {
        BiPredicate<String, String> b1 = String::startsWith;
        BiPredicate<String, String> b2 = (string, prefix) -> string.startsWith(prefix);

        System.out.println(b1.test("Benjamin", "b"));
        System.out.println(b2.test("Anna", "An"));
    }
}
