package com.code.OCP.chapter4;

import java.util.function.BinaryOperator;

public class BinaryOperatorTest {
    public static void main(String[] args) {
        BinaryOperator<String> b1 = String::concat;
        System.out.println(b1.apply("Code", "Bool"));

        BinaryOperator<String> b2 = (string, toAdd) -> string.concat(toAdd);
        System.out.println(b2.apply("Spring", "Summer"));
    }
}
