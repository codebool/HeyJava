package com.code.OCP.chapter4;

import java.util.Optional;
import java.util.stream.Stream;

public class CommonTerminalOperatorTest {
    public static void main(String[] args) {
        Stream<String> s = Stream.of("Tiger", "Monkey", "rat", "pig");
        Optional<String> min = s.min((s1, s2) -> s1.length() - s2.length());
        min.ifPresent(System.out::println);
    }
}
