package com.code.OCP.chapter4;

import java.util.function.Consumer;

public class ConsumerTest {
    public static void main(String[] args) {
        Consumer<String> c1 = System.out::println;
        c1.accept("Benjamin");

        Consumer<String> c2 = x -> System.out.println(x);
        c2.accept("Cool");
    }
}
