package com.code.OCP.chapter4;

import java.util.function.Function;

public class FunctionTest {
    public static void main(String[] args) {
        Function<String, Integer> f1 = String::length;
        int l1 = f1.apply("Benjamin");
        System.out.println(l1);

        Function<String, Integer> f2 = x -> x.length();
        int l2 = f2.apply(" R D ");
        System.out.println(l2);
    }
}
