package com.code.OCP.chapter4;

import java.util.stream.Stream;

public class ReduceTest01 {
    public static void main(String[] args) {
        String[] array = new String[] {"w", "o", "l", "f"};
        String result = "";
        for (String s : array) {
           result += s;
        }
        System.out.println(result);

        // reduce的第一个参数为起始值
        Stream<String> stream = Stream.of("w", "o", "l", "f");
        String word = stream.reduce("", (s, c) -> s + c);
        System.out.println(word);

        Stream<String> s = Stream.of("w", "o", "l", "f");
        String w = s.reduce("", String::concat);
        System.out.println(w);
    }
}
