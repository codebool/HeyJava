package com.code.OCP.chapter4;

import java.util.function.BinaryOperator;
import java.util.stream.Stream;

public class ReduceTest02 {
    public static void main(String[] args) {
        BinaryOperator<Integer> op = (a, b) -> a*b;
        Stream<Integer> empty = Stream.empty();
        Stream<Integer> one = Stream.of(1);
        Stream<Integer> three = Stream.of(3, 5, 6);

        empty.reduce(op).ifPresent(System.out::println);
        one.reduce(op).ifPresent(System.out::println);
//        three.reduce(op).ifPresent(System.out::println);
        System.out.println(three.reduce(1, op, op));
    }
}
