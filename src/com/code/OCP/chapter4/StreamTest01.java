package com.code.OCP.chapter4;

import java.util.stream.Stream;

public class StreamTest01 {
    public static void main(String[] args) {
        Stream<String> empty = Stream.empty();
        System.out.println(empty.count());

        Stream<Integer> singleElement = Stream.of(1);
        System.out.println(singleElement.count());

        Stream<Integer> fromArray = Stream.of(1, 2, 3);
        System.out.println(fromArray.count());

        Stream<Double> randoms = Stream.generate(Math::random);
//        randoms.forEach(System.out::println);

        Stream<Integer> oddNumbers = Stream.iterate(1, n-> n+2);
        System.out.println(oddNumbers);
        oddNumbers.forEach(System.out::println);
    }
}
