package com.code.OCP.chapter4;

import java.time.LocalDateTime;
import java.util.function.Supplier;

public class SupplierTest {
    public static void main(String[] args) {
        // Using method references
        Supplier<LocalDateTime> d1 = LocalDateTime::now;
        Supplier<LocalDateTime> d2 = () -> LocalDateTime.now();
        System.out.println(d1.get());
        System.out.println(d2.get());
    }
}
