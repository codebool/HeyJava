package com.code.OCP.chapter4;

import java.util.function.UnaryOperator;

public class UnaryOperatorTest {
    public static void main(String[] args) {
        // extends Function
        UnaryOperator<String> u1 = String::toUpperCase;
        System.out.println(u1.apply("benjamin"));

        UnaryOperator<String> u2 = x -> x.toUpperCase();
        System.out.println(u2.apply("anna"));
    }
}
