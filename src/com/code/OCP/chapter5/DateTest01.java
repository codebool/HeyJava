package com.code.OCP.chapter5;

import sun.security.jca.GetInstance;

import java.time.*;

public class DateTest01 {
    public static void main(String[] args) {
        System.out.println(LocalDate.now());
        System.out.println(LocalTime.now());
        System.out.println(LocalDateTime.now());
        System.out.println(ZonedDateTime.now());

        // Epoch: 1970.01.01
        System.out.println(LocalDate.ofEpochDay(1));

        Instant now = Instant.now();
        System.out.println(now);
    }
//    公元年分除以4不可整除，为平年。
//    公元年分除以4可整除但除以100不可整除，为闰年。
//    公元年分除以100可整除但除以400不可整除，为平年。
//    公元年分除以400可整除但除以3200不可整除[来源请求]，为闰年。
//
//    每逢闰年，2月分有29日，平年的2月分为28日。

//    Period
//    year, month, week, day
//    Period.of() method takes only years, months, and days
    Period period = Period.ofMonths(1);

    Duration duration = Duration.ofSeconds(3600);



}
