package com.code.OCP.chapter6;

// Clear the try with resource closed order
public class AutoClosableTest implements AutoCloseable {
    int num;

    AutoClosableTest(int num) {
        this.num = num;
    }

    public void close() {
        System.out.println("Close: " + num);
    }

    public static void main(String[] args) {
        try (AutoClosableTest a1 = new AutoClosableTest(1);
                AutoClosableTest a2 = new AutoClosableTest(2)) {
            throw new RuntimeException();
        } catch (Exception e) {
            System.out.println("ex");
        } finally {
            System.out.println("finally");
        }
    }
}
