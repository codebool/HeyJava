package com.code.OCP.chapter6;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class multiTryTest {
    public static void main(String[] args) {
        try {
            Path path = Paths.get("/Users/code/Desktop/HeyJava/src/com/code/OCP/chapter6/test.txt");
            System.out.println(path);
            String text = new String(Files.readAllBytes(path));
            System.out.println(text);
            LocalDate date = LocalDate.parse(text);
            System.out.println(date);
        } catch (DateTimeParseException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
