package com.code.OCP.chapter7;

public class ThreadTest extends Thread {
    public void run() {
        System.out.println("Printing zoo inventory");
    }

    public static void main(String[] args) {
        new ThreadTest().start();
    }
}
